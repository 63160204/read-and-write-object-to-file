/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patthamawan.readandwriteobjecttofile;
import java.io.Serializable;
/**
 *
 * @author Oil
 */
public class Player {
    
    private int win;
    private char Symbol;
    private int loss;
    private int draw;

    public int getWin() {
        return win;
    }

    public void win() {
        this.win++;
    }

    public char getSymbol() {
        return Symbol;
    }

    public int getLoss() {
        return loss;
    }

    public void loss() {
        this.loss++;
    }

    public int getDraw() {
        return draw;
    }

    public void draw() {
        this.draw++;
    }

    public Player(char Symbol) {
        this.Symbol = Symbol;
    }

    public void setSymbol(char Symbol) {
        this.Symbol = Symbol;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + Symbol + ", win=" + win + ", loss=" + loss + ", draw=" + draw + '}';
    }

}
